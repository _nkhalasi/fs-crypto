module SysCrypto

  open System
  open System.IO
  open System.Text
  open System.Security.Cryptography

  let readRsaPrivateKey filePath =
    File.ReadAllLines filePath |> Seq.toList |> List.tail |> List.map (fun x -> if x = "-----END RSA PRIVATE KEY-----" then "" else x) |> List.fold (+) ""

  let rsaPrivateKeyFrom keyContent =
    use rsa = RSA.Create()
    rsa.ImportRSAPrivateKey (ReadOnlySpan (Convert.FromBase64String keyContent)) |> ignore
    rsa.ExportParameters true

  let loadRsaPrivateKey = readRsaPrivateKey >> rsaPrivateKeyFrom

  let readRsaPublicKey filePath =
    File.ReadAllLines filePath |> Seq.toList |> List.tail |> List.map (fun x -> if x = "-----END PUBLIC KEY-----" then "" else x) |> List.fold (+) ""

  let rsaPublicKeyFrom keyContent =
    use rsa = RSA.Create()
    rsa.ImportSubjectPublicKeyInfo (ReadOnlySpan (Convert.FromBase64String keyContent)) |> ignore
    rsa.ExportParameters false

  let loadRsaPublicKey = readRsaPublicKey >> rsaPublicKeyFrom

  let sha256Hash (data : string) =
    use sha256 = SHA256.Create()
    System.Text.Encoding.UTF8.GetBytes data |> sha256.ComputeHash

  let sign (secret:string) (userId:string) (token:string) (pvtKey) =
    let message = [ "vlms"; secret; token; userId ] |> List.reduce (fun r s -> r + " :: " + s)
    printfn ("Signing: %s") message
    let msgHash = message |> sha256Hash
    printfn ("Message Hash: %s") (msgHash |> Convert.ToBase64String)
    // use rsa = RSA.Create()
    use rsa = new RSACryptoServiceProvider()
    pvtKey |> rsa.ImportParameters

    // let rsaFormatter = new RSAPKCS1SignatureFormatter(rsa)
    // rsaFormatter.SetHashAlgorithm("SHA256")
    // (rsaFormatter.CreateSignature msgHash) |> Convert.ToBase64String
        //OR
    // rsa.SignHash(msgHash, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1) |> Convert.ToBase64String
        //OR
    // rsa.SignData(Encoding.UTF8.GetBytes(message), HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1) |> Convert.ToBase64String
    rsa.SignData(Encoding.UTF8.GetBytes(message), new SHA256CryptoServiceProvider()) |> Convert.ToBase64String

  let verify (secret:string) (userId:string) (token:string) (b64Sig:string) (pubKey) =
    let message = [ "vlms"; secret; token; userId ] |> List.reduce (fun r s -> r + " :: " + s)
    printfn ("Verifying: %s") message
    let msgHash = message |> sha256Hash
    use rsa = RSA.Create()
    pubKey |> rsa.ImportParameters
    let rsaFormatter = new RSAPKCS1SignatureDeformatter(rsa)
    rsaFormatter.SetHashAlgorithm("SHA256")
    rsaFormatter.VerifySignature (msgHash, Convert.FromBase64String b64Sig)

  // let secret = "This is a secret"
  // let userId = "namesakes"
  // // let token = Guid.NewGuid().ToString()
  // let token = "5600f6c2-24c1-47b7-9cc8-53665e5a327a"

  // printfn ("%A") (loadRsaPrivateKey "private.nopass.pem")
  // printfn ("Token: %s") token

  // let signature = loadRsaPrivateKey "private.nopass.pem" |> sign secret userId token
  // printfn ("Signature: %s") signature

  // let verifyStatus = loadRsaPublicKey "public.pem" |> verify secret userId token signature
  // printfn ("Verification: %A") verifyStatus
