module BouncyCrypto

open System.IO
open System.Security.Cryptography
open System.Text
open Org.BouncyCastle
open Org.BouncyCastle.Crypto
open Org.BouncyCastle.Crypto.Parameters

let loadRsaPublicKey pemFile =
  use fs = File.OpenText(pemFile)
  let keyParam = OpenSsl.PemReader(fs).ReadObject() :?> RsaKeyParameters
  keyParam

let loadRsaPrivateKey pemFile =
  use fs = File.OpenText(pemFile)
  let keyPair = OpenSsl.PemReader(fs).ReadObject() :?> AsymmetricCipherKeyPair
  keyPair.Private

let sha256Hash (data : string) =
    use sha256 = SHA256.Create()
    Encoding.UTF8.GetBytes data |> sha256.ComputeHash

let messageHash (secret:string) (userId:string) (token:string) =
  let message = [ "vlms"; secret; token; userId ] |> List.reduce (fun r s -> r + " :: " + s)
  let hash = message |> sha256Hash
  printfn ("Message: %s") message
  printfn ("Message Hash: %s") (hash |> System.Convert.ToBase64String)
  hash

let sign (secret:string) (userId:string) (token:string) (pvtKey) =
  let msgHash = messageHash secret userId token
  let signer = Org.BouncyCastle.Security.SignerUtilities.GetSigner("SHA256withRSA")
  signer.Init(true, pvtKey)
  signer.BlockUpdate(msgHash, 0, msgHash.Length)
  signer.GenerateSignature()

let verify (secret:string) (userId:string) (token:string) (signature:string) (pubKey) =
  let msgHash = messageHash secret userId token
  let signer = Org.BouncyCastle.Security.SignerUtilities.GetSigner("SHA256withRSA")
  signer.Init(false, pubKey)
  signer.BlockUpdate(msgHash, 0, msgHash.Length)
  signer.VerifySignature(signature |> System.Convert.FromBase64String)
