open System

(*
    Generating RSA Keypair
openssl genpkey -algorithm RSA -aes256 -out private.pem
openssl rsa -in private.pem -pubout -outform PEM -out public.pem
openssl rsa -in private.pem -out private.nopass.pem

    Generating SHA256 hash
echo -n "vlms :: This is a secret :: 5600f6c2-24c1-47b7-9cc8-53665e5a327a :: namesakes" |openssl dgst -sha256 -binary |openssl enc -base64

    Signing the hash using private key
echo -n "vlms :: This is a secret :: 5600f6c2-24c1-47b7-9cc8-53665e5a327a :: namesakes" |openssl dgst -sha256 -binary |openssl dgst -sha256 -sign private.nopass.pem > signature.bin
echo -n "vlms :: This is a secret :: 5600f6c2-24c1-47b7-9cc8-53665e5a327a :: namesakes" |openssl dgst -sha256 -binary |openssl dgst -sha256 -sign private.nopass.pem |openssl enc -base64

    Verifying the signature using public key
echo -n "vlms :: This is a secret :: 5600f6c2-24c1-47b7-9cc8-53665e5a327a :: namesakes" |openssl dgst -sha256 -binary | openssl dgst -sha256 -verify public.pem -signature signature.bin

*)

[<EntryPoint>]
let main argv =

    let secret = "This is a secret"
    let userId = "namesakes"
    // let token = Guid.NewGuid().ToString()
    let token = "5600f6c2-24c1-47b7-9cc8-53665e5a327a"

    // printfn ("%A") (SysCrypto.loadRsaPrivateKey "private.nopass.pem")
    // printfn ("Token: %s") token

    // let signature = SysCrypto.loadRsaPrivateKey "private.nopass.pem" |> SysCrypto.sign secret userId token
    // printfn ("Signature: %s") signature

    // let verifyStatus = SysCrypto.loadRsaPublicKey "public.pem" |> SysCrypto.verify secret userId token signature
    // printfn ("Verification: %A") verifyStatus

    printfn ("Public Key: %A") (BouncyCrypto.loadRsaPublicKey "public.pem")
    printfn ("Private Key: %A") (BouncyCrypto.loadRsaPrivateKey "private.nopass.pem")

    let signature = BouncyCrypto.loadRsaPrivateKey "private.nopass.pem" |> BouncyCrypto.sign secret userId token |> Convert.ToBase64String
    printfn ("Signature: %s") signature

    printfn ("Verification : %A") (BouncyCrypto.loadRsaPublicKey "public.pem" |> BouncyCrypto.verify secret userId token signature)

    0 // return an integer exit code
